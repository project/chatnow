<?php
/**
 * @file
 * Admin page callback for the ChatNow module.
 */

/**
 * Builds and returns the ChatNow settings form.
 */
function chatnow_admin_settings() {
  $form['chatnow_example_setting'] = array(
    '#type' => 'textarea',
    '#title' => t('Example setting'),
    '#default_value' => variable_get('chatnow_example_setting', ''),
    '#description' => t('This is an example setting.'),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}
